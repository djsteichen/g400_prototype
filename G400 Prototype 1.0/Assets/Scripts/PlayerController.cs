﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening; 

public class PlayerController : MonoBehaviour {
    //public float thrustSpeed;
    private Rigidbody2D ballRb;
    public float bounceHeight;
    public float dashSpeed;
    private float dashTime; //how long dash lasts
    public float startDashTime;
    private int direction;
    private bool canDash = true;
    public GameObject mainCam;

    //public GameObject winText;

    public float xMin, xMax;

    public GameObject dashEffect; //particle effect

	void Start () {
        //Physics.gravity = new Vector3(0, -500.0F, 0); //change the intensity of gravity
        ballRb = GetComponent<Rigidbody2D>();
        dashTime = startDashTime;
        
	}
	
    void FixedUpdate()
    {
        ballRb.position = new Vector3
        (
            Mathf.Clamp(ballRb.position.x, xMin, xMax),
            ballRb.position.y,
            0.0f
        );
    }

    void Update() //need to bound player to edges of screen - see SpaceShooter tutorial
    {
       
        if (direction == 0)
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow) && canDash)
            {
                direction = 1;
                canDash = false;
                Debug.Log("canDash set to " + canDash);
                mainCam.transform.DOShakePosition(0.1f, 0.25f, 10, 80.0f, false, true); //shake cam
                Instantiate(dashEffect, transform.position, Quaternion.identity); //spawn particles
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow) && canDash)
            {
                direction = 2;
                canDash = false;
                Debug.Log("canDash set to " + canDash);
                mainCam.transform.DOShakePosition(0.1f, 0.25f, 10, 80.0f, false, true); //shake cam
                Instantiate(dashEffect, transform.position, Quaternion.identity); //spawn particles
            }
        }

        else
        {
            if (dashTime <= 0)
            {
                direction = 0;
                dashTime = startDashTime;
                ballRb.velocity = Vector2.zero;
            }
            else
            {
                dashTime -= Time.deltaTime;
            }
            if (direction == 1)
            {
                ballRb.velocity = Vector2.left * dashSpeed;
                
                
            }
            if (direction == 2)
            {
                ballRb.velocity = Vector2.right * dashSpeed;
            }
        }
               

        //Debug.Log(ballRb.angularVelocity);
        /*if (Input.GetKeyDown("right")) //this.transform.position.x to keep track of the ball's x pos
        {
            Vector2 dashRight = new Vector2(5.0f, 0.0f);
            ballRb.velocity = dashRight;
            /*Vector2 dashRight = new Vector2(2.0f,this.transform.position.y);
            //ballRb.AddForce(transform.right * thrustSpeed);
            ballRb.DOMove(dashRight,0.5f, false);
            Debug.Log("RIGHT key press registered!");
        }
        if (Input.GetKeyDown("left"))
        {
            //ballRb.AddForce(-transform.right * thrustSpeed);
            Vector2 dashLeft = new Vector2(2.0f, this.transform.position.y);
            ballRb.DOMove(dashLeft, 0.5f, false);
            Debug.Log("LEFT key press registered!");
        }
        /*float moveHorizontal = Input.GetAxis("Horizontal");
        Vector3 movement = new Vector3(moveHorizontal, Physics.gravity.y, 0.0f);
        ballRb.velocity = movement * thrustSpeed;*/
    }

    /*void OnTriggerEnter(Collider other)
    {
        Vector3 trampoline = new Vector3(0.0f, 50.0f);
        ballRb.AddForce(trampoline);
    }*/
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Platform") && ballRb.velocity.y <=0) //need to figure out how to limit this
        {
            Vector2 v2 = new Vector2(0, bounceHeight);
            Debug.Log("collision detected");
            ballRb.AddForce(v2);
            canDash = true;
            
        }
        /*if (other.gameObject.CompareTag("Finish"))
        {
            winText.visible = true;
        }*/
    }
}
